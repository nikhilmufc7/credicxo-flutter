import 'package:connectivity/connectivity.dart';
import 'package:credixco_Flutter/BLoC/movie_detail_bloc_provider.dart';
import 'package:credixco_Flutter/BLoC/popular_movies_bloc.dart';
import 'package:credixco_Flutter/models/movies.dart';
import 'package:credixco_Flutter/ui/movie_detail.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    bloc.getMovies();
  }

  @override
  void dispose() {
    super.dispose();
    bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurpleAccent,
        title: Text('Popular Movies'),
      ),
      body: StreamBuilder(
        stream: Connectivity().onConnectivityChanged,
        builder: (ctx, AsyncSnapshot snapConnectivity) {
          if (snapConnectivity.data == null) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          var connectivityResult = snapConnectivity.data;

          switch (connectivityResult) {
            case ConnectivityResult.mobile:
            case ConnectivityResult.wifi:
              return StreamBuilder(
                stream: bloc.allMovies,
                builder: ((context, snapshot) {
                  if (snapshot.hasData) {
                    return listOfMovies(snapshot);
                  } else if (snapshot.hasError) {
                    print("Some error occured");
                    return Text(snapshot.error.toString());
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }),
              );
            case ConnectivityResult.none:
            default:
              return Center(
                child: Text('No Internet, Please try again'),
              );
          }
        },
      ),
    );
  }

  Widget listOfMovies(AsyncSnapshot<Movies> snapshot) {
    return ListView.builder(
      itemCount: snapshot.data.results.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListTile(
            onTap: () => onTapFunc(snapshot.data, index),
            leading: Image.network(
                'https://image.tmdb.org/t/p/w185/${snapshot.data.results[index].poster_path}'),
            title: Text(snapshot.data.results[index].title),
            subtitle: Text(snapshot.data.results[index].title),
            trailing: Icon(Icons.favorite),
          ),
        );
      },
    );
  }

  onTapFunc(Movies data, int index) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return MovieDetailBlocProvider(
        child: MovieDetailScreen(
          name: data.results[index].title,
          movieId: data.results[index].id,
        ),
      );
    }));
  }
}
