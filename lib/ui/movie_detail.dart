import 'package:credixco_Flutter/BLoC/movie_data_bloc.dart';
import 'package:credixco_Flutter/BLoC/movie_detail_bloc_provider.dart';
import 'package:credixco_Flutter/models/movieData.dart';

import 'package:flutter/material.dart';

class MovieDetailScreen extends StatefulWidget {
  final String name;

  final int movieId;

  MovieDetailScreen({
    this.name,
    this.movieId,
  });

  @override
  _MovieDetailScreenState createState() => _MovieDetailScreenState();
}

class _MovieDetailScreenState extends State<MovieDetailScreen> {
  MovieDataBloc bloc;
  final List<String> resultsList = List<String>();

  @override
  void didChangeDependencies() {
    bloc = MovieDetailBlocProvider.of(context).bloc;

    bloc.fetchMovieDetailById(widget.movieId);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(margin: EdgeInsets.only(top: 8.0, bottom: 8.0)),
            Text(widget.name),
            Container(margin: EdgeInsets.only(top: 8.0, bottom: 8.0)),
            StreamBuilder(
              stream: bloc.movieDetail,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return FutureBuilder(
                    future: snapshot.data,
                    builder: (context, AsyncSnapshot<MovieData> snapshot) {
                      if (snapshot.hasData) {
                        var results = snapshot.data.results;
                        return ListView.builder(
                            shrinkWrap: true,
                            itemCount: results.length,
                            itemBuilder: (context, index) {
                              return ListTile(
                                title: Text(results[index].name),
                                subtitle: Text(results[index].type),
                                leading: Icon(Icons.video_library),
                              );
                            });
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
