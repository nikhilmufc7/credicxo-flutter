import 'dart:async';
import 'package:credixco_Flutter/api/get_movies_api.dart';
import 'package:credixco_Flutter/models/movieData.dart';

import 'package:rxdart/rxdart.dart';

class MovieDataBloc {
  final _getMovies = GetMoviesApi();
  final _movieId = PublishSubject<int>();
  final _movieData = BehaviorSubject<Future<MovieData>>();

  Function(int) get fetchMovieDetailById => _movieId.sink.add;
  Stream<Future<MovieData>> get movieDetail => _movieData.stream;

  MovieDataBloc() {
    _movieId.stream.transform(_movieDetailTransformer()).pipe(_movieData);
  }

  dispose() async {
    _movieId?.close();
    _movieData?.close();
  }

  _movieDetailTransformer() {
    return ScanStreamTransformer(
      (Future<MovieData> movieDetail, int id, int index) {
        movieDetail = _getMovies.fetchMovieData(id);
        return movieDetail;
      },
    );
  }
}
