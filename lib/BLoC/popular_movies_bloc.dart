import 'package:connectivity/connectivity.dart';
import 'package:credixco_Flutter/api/get_movies_api.dart';
import 'package:credixco_Flutter/models/movies.dart';
import 'package:rxdart/rxdart.dart';

class PopularMoviesBloc {
  final _getMovies = GetMoviesApi();

  final _fetchMoviesFromApi = PublishSubject<Movies>();

  Stream<Movies> get allMovies => _fetchMoviesFromApi.stream;

  getMovies() async {
    Movies movieslist = await _getMovies.fetchMovieList();
    _fetchMoviesFromApi.sink.add(movieslist);
  }

  dispose() {
    _fetchMoviesFromApi?.close();
  }
}

final bloc = PopularMoviesBloc();
