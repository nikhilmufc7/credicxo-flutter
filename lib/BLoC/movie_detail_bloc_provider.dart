import 'package:credixco_Flutter/BLoC/movie_data_bloc.dart';

import 'package:flutter/material.dart';

class MovieDetailBlocProvider extends InheritedWidget {
  final MovieDataBloc bloc;

  MovieDetailBlocProvider({Key key, Widget child})
      : bloc = MovieDataBloc(),
        super(key: key, child: child);

  static MovieDetailBlocProvider of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType();

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}
