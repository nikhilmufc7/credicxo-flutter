class Movies {
  List<_Result> _results = [];

  Movies.fromJson(Map<String, dynamic> parsedJson) {
    print(parsedJson['results'].length);

    List<_Result> temp = [];
    for (int i = 0; i < parsedJson['results'].length; i++) {
      _Result result = _Result(parsedJson['results'][i]);
      temp.add(result);
    }
    _results = temp;
  }

  List<_Result> get results => _results;
}

class _Result {
  int _id;

  String _title;

  String _poster_path;

  _Result(result) {
    _id = result['id'];

    _title = result['title'];

    _poster_path = result['poster_path'];
  }

  String get poster_path => _poster_path;

  String get title => _title;

  int get id => _id;
}
