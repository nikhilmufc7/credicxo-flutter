class MovieData {
  List<DetailResult> _results = [];

  MovieData({
    List<DetailResult> results,
  }) {
    this._results = results;
  }
  List<DetailResult> get results => _results;
  set results(List<DetailResult> results) => _results = results;

  MovieData.fromJson(Map<String, dynamic> json) {
    if (json['results'] != null) {
      _results = new List<DetailResult>();
      json['results'].forEach((v) {
        _results.add(DetailResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this._results != null) {
      data['results'] = this._results.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class DetailResult {
  String _id;
  String _name;
  String _type;

  DetailResult({String id, String name, String type}) {
    this._id = id;
    this._name = name;
    this._type = type;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get type => _type;
  set type(String type) => _type = type;

  DetailResult.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['type'] = this._type;
    return data;
  }
}
