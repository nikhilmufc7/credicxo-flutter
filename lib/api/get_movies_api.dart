import 'dart:async';

import 'dart:io';

import 'package:credixco_Flutter/models/movieData.dart';
import 'package:credixco_Flutter/models/movies.dart';
import 'package:http/http.dart' as http;

import 'dart:convert';

class GetMoviesApi {
  http.Client client = http.Client();
  final _apiKey = '22cda16953876cd5324e482696d8a60c';

  Future<Movies> fetchMovieList() async {
    try {
      final res = await client
          .get("http://api.themoviedb.org/3/movie/popular?api_key=$_apiKey");
      print(res.body.toString());
      if (res.statusCode == 200) {
        print("Successful");
        return Movies.fromJson(json.decode(res.body));
      } else {
        print("  Not successful ${res.statusCode}");
        throw Exception('Not able to load movies');
      }
    } catch (e, stackTrace) {
      if (e is SocketException) {
        print('No Internet Connection');
      }
    }
  }

  Future<MovieData> fetchMovieData(int movieId) async {
    try {
      final res = await client.get(
          "http://api.themoviedb.org/3/movie/$movieId/videos?api_key=22cda16953876cd5324e482696d8a60c");
      print(res.body.toString());
      if (res.statusCode == 200) {
        return MovieData.fromJson(json.decode(res.body));
      } else {
        throw Exception('Not able to load');
      }
    } catch (e, stackTrace) {
      if (e is SocketException) {
        print('No Internet Connection');
      }
    }
  }
}
